<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//
//Route::group(array('domain' => 'freeleads.dev'), function()
//{
//    dd('test');
//});
//
//Route::group(array('domain' => 'education.freeleads.dev'), function()
//{
//    dd('haha');
//});

//Route::group(['domain' => 'freeleads.dev'], function(){
//    dd('haha');
//});
//
//Route::group(['domain' => 'education.freeleads.dev'], function(){
//    dd('hehe');
//});
//
//
//Route::group(['domain' => 'freeleads.engageiq.com'], function(){
//    dd('freeleads');
//});
//
//
//
//Route::group(['domain' => 'freeleads.engageiq.com'], function(){
//   dd('freeleads');
//});
//
//Route::group(['domain' => 'education.engageiq.com'], function(){
//    dd('education');
//});
//
//Route::group(['domain' => 'test.engageiq.com'], function(){
//    dd('test');
//});


Route::get('/', function () {
    return view('index')->withSubDomain(strtoupper(explode('.', $_SERVER['HTTP_HOST'])[0]));
});

Route::get('/test', function () {
    return view('test');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
