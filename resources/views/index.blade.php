<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Engage IQ PPC</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<div class="main-navigation">
    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <img src="images/img2.png" alt="">
                <div class="contact-head">
                    <p>Speak with Our Experts and Find Out<br>How You Can Get 100 Free Leads Today.</p>
                    <a class="phone" href="#"><strong>1-800-503-1028</strong></a>
                </div>             
            </div>
        </div>
    </div>
</div>
<div id="index-banner" class="custom-pa">
    <div class="section no-pad-bot home-banner">
        <div class="container">
            <div class="row">
                <div class="col s12 m8">
                    <h1>Get 100 Fresh, Verified,<br>{{ $sub_domain }} Leads Today, Free!</h1>
                    <h3>Remove the Biggest Barriers &amp; Risks to Growing Your<br>Business With {{ $sub_domain }} Lead.</h3>
                    <div class="list-service">
                        <img src="images/img4.png" alt="">
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="person-magnet-image">
                        <img src="images/img3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-1">
    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <div class="trusted">
                    <p>Trusted By</p>
                    <div class="clients">
                        <img src="images/client-logo1.jpg" alt="">
                        <img src="images/client-logo2.jpg" alt="">
                        <img src="images/client-logo4.jpg" alt="">
                        <img src="images/client-logo3.jpg" alt="">                    
                    </div>
                    <div class="clients">
                        <img src="images/client-logo7.jpg" alt="">
                        <img src="images/client-logo6.jpg" alt="">                      
                        <img src="images/client-logo5.jpg" alt="">
                        <img src="images/client-logo8.jpg" alt="">
                        <img src="images/client-logo9.jpg" alt="">
                    </div>
                </div>
                <div class="smart-industry">
                    <h3>Why Smart {{ $sub_domain }} Companies Switch<br>
                        to <span>Engage <span>IQ</span></span> Lead Generation Services</h3>
                </div>
            </div>
        </div>
        <div class="row perks">
            <div class="col s12 m3">
                <img src="images/img6-2.png" alt="">
                <p><strong>Try Before You Buy:</strong> Try every lead<br>
                    for 30 days, risk free. If the information<br>
                    is incorrect, you’ll get a<br>
                    refund, no questions asked.</p>
            </div>
            <div class="col s12 m3">
                <img src="images/img8-2.png" alt="">
                <p><strong>No Volume Requirements:</strong> Need<br>
                    a few leads? Or a few 100,000?<br>
                    No matter your requirements<br>
                    we’ll help you find all the customers<br>
                    you need to raise their hand<br>
                    and buy.</p>
            </div>
            <div class="col s12 m3">
                <img src="images/img7-2.png" alt="">
                <p><strong>100% Transparency:</strong> We show<br>
                    you the exact survey website<br>
                    where your lead was produced so<br>
                    you can guarantee it meets your<br>
                    quality standards.</p>
            </div>
            <div class="col s12 m3">
                <img src="images/img9-2.png" alt="">
                <p><strong>No credit card required. No lead<br>
                    purchase minimums.</strong> No hassle.</p>
            </div>
        </div>
    </div>
</div>

<div class="section-3">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 desktop-content">
                <img src="images/img10.png" alt="">
            </div>
            <div class="col s12 m12 mobile-content">
                <h3>REMOVE THE RISK</h3>
                <p>And Finally Grow Your {{ $sub_domain }}<br>
Business Easily, Quickly,<br>
Effectively & Without Hassle.</p>
                <div class="risk">
                    <h1>01</h1>
                    <h2>THE SEARCH</h2>
                    <p>Are you looking for the most effective online marketing
strategy to profitably grow your business? You’ve likely
heard that you need to generate customized leads.</p>
                </div>
                <div class="risk">
                    <h1>02</h1>
                    <h2>THE RISK</h2>
                    <p>While that is unquestionably the proven way to grow your
business fast, handling all of the strategy, technology, and
lead vetting in-house is time consuming, costly and risky.</p>
                </div>
                <div class="risk">
                    <h1>03</h1>
                    <h2>THE SOLUTION</h2>
                    <p>We remove 100% of the risk of online lead generation
for {{ $sub_domain }} with customized, fresh, Artificial Intelligence
verified leads that drop into your reality seconds
after your customer shows declares an intent to buy.</p>
                </div>
                <div class="risk">
                    <h1>04</h1>
                    <h2>THE COMPANY</h2>
                    <p>With 12 years of proven industry experience, and a team
that has a unique understanding of your {{ $sub_domain }} business,
you can rest assured that EngageIQ will constantly
innovate and deliver the leads you need through our customized
cost-per-action platforms.</p>
                </div>
                <div class="risk">
                    <h1>05</h1>
                    <h2>THE ACTION</h2>
                    <p>Contact us today to discover the most effective way to
reach your targeted, active audience of intent-driven
buyers, and get 100 free leads on us.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-4 simple-steps">
    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <h2>Just 3 Simple Steps to<br>
                    Profitable Cost-Per-Acquisition Leads</h2>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m4">
                <h4>Step 1</h4>
                <h3>CONNECT</h3>
                <p>Call or chat with us to get your first 100 leads free,
                    and discuss how we can bolster your business
                    revenue, no obligation or credit card required.</p>
            </div>
            <div class="col s12 m4">
                <h4>Step 2</h4>
                <h3>STRATEGIZE</h3>
                <p>Discuss how we can customize your exclusive
                    campaign to send the highest quality, most profitable
                    leads straight to your inbox.</p>
            </div>
            <div class="col s12 m4">
                <h4>Step 3</h4>
                <h3>CAMPAIGN SETUP</h3>
                <p>We handle complicated technology and nitty-gritty
                    details. You simply sit back, receive the lead, call
                    your next customer, and close the deal.</p>
            </div>
        </div>
    </div>
</div>

<!-- <footer class="footer">
    <a href="#">Questions?</a>
    <br>
    <a href="#">Contact Us</a>
</footer> -->

<!-- <a href="#" class="chatnow">CHAT WITH US NOW!</a> -->


<!--  Scripts-->


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?5FNkdjtANnLlYW6XiDYZ2e0miIKE7EVp";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>


</body>
</html>
